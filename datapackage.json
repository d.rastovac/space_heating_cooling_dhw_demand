{
  "zenodoId": 4686964,
  "doi": "10.5281/zenodo.4686964",
  "id": "24393cb0-f308-4c36-9142-4e68256156ba",
  "name": "space_heating_cooling_dhw_demand",
  "title": "Space heating, cooling and DHW demand",
  "profile": "tabular-data-resource",
  "description": "Data show Space heating, cooling and DHW demand composition in the EU28 at NUTS0 level.",
  "version": "0.1",
  "created": "2018-03-09",
  "keywords": [
    "Space heating",
    "Cooling",
    "DHW",
    "Europe",
    "EU28"
  ],
  "licenses": [
    {
      "id": "CC-BY-4.0",
      "title": "Creative Commons Attribution 4.0 International",
      "url": "https://creativecommons.org/licenses/by/4.0/legalcode"
    }
  ],
  "sources": [
    {
      "title": "See comments in the XLSX files",
      "path": "data/data/space_heating_cooling_dhw_bottom-up.xlsx"
    },
    {
      "title": "See comments in the XLSX files",
      "path": "data/space_heating_cooling_dhw_bottom-up_SH+DHW.xlsx"
    },
    {
      "title": "See comments in the XLSX files",
      "path": "data/space_heating_cooling_dhw_top-down.xlsx"
    }
  ],
  "contributors": [
    {
      "title": "Simon Pezzutto",
      "email": "Simon.Pezzutto@eurac.edu",
      "role": "author",
      "organization": "Eurac Research"
    },
    {
      "title": "Stefano Zambotti",
      "email": "Stefano.Zambottio@eurac.edu",
      "role": "author",
      "organization": "Eurac Research"
    },
    {
      "title": "Silvia Croce",
      "email": "Silvia.Croce@eurac.edu",
      "role": "author",
      "organization": "Eurac Research"
    },
    {
      "title": "Pietro Zambelli",
      "email": "Pietro.Zambelli@eurac.edu",
      "role": "author",
      "organization": "Eurac Research"
    }
  ],
  "resources": [
    {
      "path": "data/space_heating_cooling_dhw_bottom-up.csv",
      "profile": "tabular-data-resource",
      "name": "space_heating_cooling_dhw_bottom-up",
      "title": "Space heating, cooling and DHW demand - Bottom-up",
      "description": "The data shows the Space heating, cooling and DHW demand at EU28 scale (Bottom-up) visualization",
      "format": "csv",
      "mediatype": "text/csv",
      "encoding": "utf-8",
      "dialect": {
        "delimiter": "|",
        "doubleQuote": true,
        "lineTerminator": "\n",
        "header": true
      },
      "schema": {
        "fields": [
          {
            "name": "input_data",
            "type": "string",
            "format": "default",
            "unit": ""
          },
          {
            "name": "topic",
            "type": "string",
            "format": "default",
            "unit": ""
          },
          {
            "name": "sector",
            "type": "string",
            "format": "default",
            "unit": ""
          },
          {
            "name": "ac_type",
            "type": "string",
            "format": "default",
            "unit": ""
          },
          {
            "name": "type",
            "type": "string",
            "format": "default",
            "unit": ""
          },
          {
            "name": "value",
            "type": "number",
            "format": "default",
            "unit": ""
          },
          {
            "name": "unit",
            "type": "string",
            "format": "default",
            "unit": "unit"
          }
        ],
        "missingValues": [
          ""
        ]
      }
    },
    {
      "name": "space_heating_cooling_dhw_bottom-up_sh_dhw",
      "title": "Space heating, cooling and DHW demand - Bottom-up (SH+DHW)",
      "path": "data/space_heating_cooling_dhw_bottom-up_SH+DHW.csv",
      "profile": "tabular-data-resource",
      "format": "csv",
      "mediatype": "text/csv",
      "encoding": "utf-8",
      "spatial_resolution": "NUTS_0",
      "spatial_key_field": "country_code",
      "dialect": {
        "delimiter": "|",
        "doubleQuote": true,
        "lineTerminator": "\n",
        "header": true
      },
      "schema": {
        "fields": [
          {
            "name": "country",
            "type": "string",
            "format": "default",
            "unit": ""
          },
          {
            "name": "country_code",
            "description": "Country code [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2)",
            "type": "string",
            "format": "default",
            "unit": ""
          },
          {
            "name": "technology",
            "type": "string",
            "format": "default",
            "unit": ""
          },
          {
            "name": "technology_type",
            "type": "string",
            "format": "default",
            "unit": ""
          },
          {
            "name": "topic",
            "type": "string",
            "format": "default",
            "unit": ""
          },
          {
            "name": "metric",
            "type": "string",
            "format": "default",
            "unit": ""
          },
          {
            "name": "value",
            "type": "number",
            "format": "default",
            "unit": ""
          },
          {
            "name": "unit",
            "type": "string",
            "format": "default",
            "unit": "unit"
          },
          {
            "name": "estimated",
            "type": "integer",
            "format": "default",
            "unit": ""
          },
          {
            "name": "comment",
            "type": "string",
            "format": "default",
            "unit": ""
          }
        ],
        "missingValues": [
          ""
        ]
      }
    },
    {
      "path": "data/space_heating_cooling_dhw_top-down.csv",
      "name": "space_heating_cooling_dhw_top-down",
      "title": "Space heating, cooling and DHW demand - Top-down",
      "description": "The data shows the Space heating, cooling and DHW demand at EU28 scale (Top-down)",
      "profile": "tabular-data-resource",
      "format": "csv",
      "mediatype": "text/csv",
      "encoding": "utf-8",
      "spatial_resolution": "NUTS_0",
      "spatial_key_field": "country_code",
      "dialect": {
        "delimiter": "|",
        "doubleQuote": true,
        "lineTerminator": "\n",
        "header": true
      },
      "schema": {
        "fields": [
          {
            "name": "country",
            "type": "string",
            "format": "default",
            "unit": ""
          },
          {
            "name": "country_code",
            "description": "Country code [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2)",
            "type": "string",
            "format": "default",
            "unit": ""
          },
          {
            "name": "type",
            "type": "string",
            "format": "default",
            "unit": ""
          },
          {
            "name": "topic",
            "type": "string",
            "format": "default",
            "unit": ""
          },
          {
            "name": "feature",
            "type": "string",
            "format": "default",
            "unit": ""
          },
          {
            "name": "value",
            "type": "number",
            "format": "default",
            "unit": ""
          },
          {
            "name": "unit",
            "type": "string",
            "format": "default",
            "unit": "unit"
          },
          {
            "name": "estimated",
            "type": "integer",
            "format": "default",
            "unit": ""
          }
        ],
        "missingValues": [
          ""
        ]
      }
    },
    {
      "profile": "tabular-data-resource",
      "name": "DHW_space_HC",
      "title": "Domestic Hot Water and space Heating & Cooling",
      "path": "data/DHW_spaceHC.csv",
      "description": "Qualitative Domestic How Water and Space Heating and Cooling system.",
      "format": "csv",
      "mediatype": "text/csv",
      "encoding": "utf-8",
      "spatial_resolution": "NUTS_0",
      "spatial_key_field": "country_code",
      "dialect": {
        "delimiter": "|",
        "doubleQuote": true,
        "lineTerminator": "\n",
        "header": true
      },
      "schema": {
        "fields": [
          {
            "name": "country",
            "description": "Country name",
            "type": "string",
            "unit": ""
          },
          {
            "name": "country_code",
            "description": "Country code [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2)",
            "type": "string",
            "unit": ""
          },
          {
            "name": "sector",
            "description": "Main sectors: residential, service",
            "type": "string",
            "unit": ""
          },
          {
            "name": "subsector",
            "description": "Main subsectors: Total, Single family- Terraced houses, Multifamily houses, Appartment blocks (> 8 floors), Offices, Trade, Education, Hospital, Hotel, Hotels and Restaurants, Other non-residential buildings",
            "type": "string",
            "unit": ""
          },
          {
            "name": "btype",
            "description": "Building type: Total, Single family- Terraced houses, Multifamily houses, Appartment blocks (> 8 floors), Offices, Trade, Education, Hospital, Hotel, Hotels and Restaurants, Other non-residential buildings",
            "type": "string",
            "unit": ""
          },
          {
            "name": "bage",
            "description": "Building epoch of construction: Before 1945, 1945 - 1969, 1970 - 1979, 1980 - 1989, 1990 - 1999, 2000 - 2010, Post 2010",
            "type": "string",
            "unit": ""
          },
          {
            "name": "topic",
            "description": "Other building features: Area, Technologies, Construction features, Energy demand, Total energy demand, Total energy consumption",
            "type": "string",
            "unit": ""
          },
          {
            "name": "feature",
            "description": "",
            "type": "string",
            "unit": ""
          },
          {
            "name": "type",
            "description": "",
            "type": "string",
            "unit": ""
          },
          {
            "name": "detail",
            "description": "Other building features: Number of dwellings/units[Mil.], Space heating, Space cooling, Domestic hot water (DHW), Covered area: constructed [Mm\u00b2], Covered area: heated [Mm\u00b2], Covered area: cooled [Mm\u00b2], Walls (u-values, construction methodology, construction materials), Windows (u-values, construction methodology, construction materials) As comment: single glass, double glass and box type window, Roof (u-values, construction methodology, construction materials) As comment: flat roof or saddle roof, Floor (u-values, construction methodology, construction materials), Space heating [kWh/m\u00b2 year], Space cooling [kWh/m\u00b2 year], Domestic hot water (DHW) [kWh/m\u00b2 year], Thermal [TWh/year], Number of buildings[Mil.]",
            "type": "string",
            "unit": ""
          },
          {
            "name": "value",
            "type": "string",
            "unit": ""
          },
          {
            "name": "unit",
            "type": "string",
            "unit": "unit"
          }
        ]
      }
    }
  ]
}